import { Component, OnInit, TemplateRef } from '@angular/core';
import { UserServiceService } from '../../data/user-service.service';
import { IUser, IContact } from '../../data/imodel';
// import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ViewContactComponent } from '../view-contact/view-contact.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  modalRef: BsModalRef;
  message: string;
  numOfUsers: IUser[];
  numOfContacts: IContact[];
  closeResult: string;
  private contactlist: IContact[];


  constructor(private userSvc: UserServiceService,private modalService: BsModalService,private modlService: NgbModal) { }

  ngOnInit() {
    this.allUsers();
  }
  allUsers(){
    this.userSvc.getAllUsers().subscribe(data => {
     this.numOfUsers = data;
     console.log(data);

     for(var i = 0;i<this.numOfUsers.length;i++){

       this.userSvc.getContactById(this.numOfUsers[i].id).subscribe(x =>{
         console.log(x);
        // this.numOfContacts = x.length;
        // console.log('this.numOfContacts:' + this.numOfContacts);
       })

     }

    })
  }

  deleteAccount(id) {

    this.userSvc.delete(id).subscribe(data => {
      console.log(data);
      location.reload();
      this.getDismissReason(this.closeResult);
    });
  }
  open(content) {
    this.modlService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  getContact(id,name) {
    console.log(name);
    //setContactName
    this.userSvc.setContactName(name);

      this.userSvc.getContactById(id).subscribe((data:IContact[]) => {
        console.log(data);
        this.userSvc.setContactList(data);

      })
      // this.userSvc.getContactList().subscribe( data => {
      //   console.log(data);
      //   this.contactlist = data;
      // })
      this.modlService.open(ViewContactComponent, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });

  }
}
