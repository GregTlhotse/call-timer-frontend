import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../../data/user-service.service';
import { IUser, IContact } from '../../data/imodel';

@Component({
  selector: 'app-user-contact-list',
  templateUrl: './user-contact-list.component.html',
  styleUrls: ['./user-contact-list.component.scss']
})
export class UserContactListComponent implements OnInit {

  numOfUsers: IUser[];
  numOfContacts: IContact[];
  constructor(private userSvc: UserServiceService) { }

  ngOnInit() {
    this.allUsers();
  }
  allUsers(){
    this.userSvc.getAllUsers().subscribe(data => {
     this.numOfUsers = data;
     console.log(data);

     for(var i = 0;i<this.numOfUsers.length;i++){

       this.userSvc.getContactById(this.numOfUsers[i].id).subscribe(x =>{
         console.log(x);
        // this.numOfContacts = x.length;
        // console.log('this.numOfContacts:' + this.numOfContacts);
       })

     }

    })
  }

}
