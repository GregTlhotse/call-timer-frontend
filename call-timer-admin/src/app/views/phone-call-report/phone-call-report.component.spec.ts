import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneCallReportComponent } from './phone-call-report.component';

describe('PhoneCallReportComponent', () => {
  let component: PhoneCallReportComponent;
  let fixture: ComponentFixture<PhoneCallReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneCallReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneCallReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
