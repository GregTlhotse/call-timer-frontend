import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../../data/user-service.service';
import { ICallLog } from '../../data/imodel';
import * as jspdf from 'jspdf';  
  
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-phone-call-report',
  templateUrl: './phone-call-report.component.html',
  styleUrls: ['./phone-call-report.component.scss']
})
export class PhoneCallReportComponent implements OnInit {
calls: ICallLog[];
searchTerm: string = '';
allCalls: ICallLog[];
noResults: boolean = false;

  constructor(private userSvc: UserServiceService) { }

  ngOnInit() {
    this.callLog();
  }
callLog(){
  this.userSvc.getCallLog().subscribe(x => {
    this.calls = x;
    console.table(x);

  })
}
searchContact() {
  // this.searchTerm = ev.target.value;
  console.log(this.searchTerm);
  this.userSvc.getCallLog().subscribe( (data:ICallLog[]) => {
   this.calls = data;
   this.allCalls = data;
   this.noResults = false;

   if(!this.searchTerm){
     console.log(this.allCalls);
     return this.calls = this.allCalls;
   }
   this.calls = this.allCalls.filter((item) => {
            return item.callFromName.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
       });
       console.log(this.calls);
       if(this.calls.length === 0){
         console.log("Success");
        this.noResults = true;

       }

 })
}

orderByDate() {
  this.userSvc.getOrderByDate().subscribe(data => {
    console.log(data);
    this.calls = data;
  })
}
orderByDuration() {
  this.userSvc.getOrderByDuration().subscribe(data => {
    console.log(data);
    this.calls = data;
  })
}
captureScreen()  
{  
  var data = document.getElementById('contentToConvert');  
  html2canvas(data).then(canvas => {  
    // Few necessary setting options  
    var imgWidth = 208;   
    var pageHeight = 295;    
    var imgHeight = canvas.height * imgWidth / canvas.width;  
    var heightLeft = imgHeight;  

    const contentDataURL = canvas.toDataURL('image/png')  
    let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
    var position = 0;
    pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
    pdf.save('CallTimerReport.pdf'); // Generated PDF   
  });  
} 
}
