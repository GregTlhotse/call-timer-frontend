import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserServiceService } from '../../data/user-service.service';
import { IServiceProvider } from '../../data/imodel';
import { UpdateServiceProviderComponent } from '../../update-service-provider/update-service-provider.component';

@Component({
  selector: 'app-service-provider-list',
  templateUrl: './service-provider-list.component.html',
  styleUrls: ['./service-provider-list.component.scss']
})
export class ServiceProviderListComponent implements OnInit {
  closeResult: string;
  myForm: FormGroup;
  serviceProviders: IServiceProvider[];

  constructor(private modlService: NgbModal,private formBuilder: FormBuilder,private toastr: ToastrService,
    private userSvc: UserServiceService) { }

  ngOnInit() {
    this.createForm();
    this. getServiceProvider();
  }
  open(content) {
    this.modlService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      serviceProviderName: ['', [Validators.required,
       ]],
       serviceProviderUSSD: ['', [Validators.required]],
    });
  }
  submitForm () {
    if (!this.myForm.valid) {
      console.log(this.myForm.value)
      return this.toastr.error('Please fill in all fields!', 'Oops!');
    }
    this.userSvc.saveServiceProvider(this.myForm.value).subscribe(x =>{
      console.log(x);
      setTimeout(() =>{
        location.reload();
      },3000);
      this.modlService.dismissAll();
       this.toastr.success('Service Provider Saved!', 'Success!');

    })
  }
  getServiceProvider() {
    this.userSvc.getServiceProvider().subscribe(data => {
      console.log(data);
      this.serviceProviders = data;
      //spID
    })
  }
  showEdit(id){
    localStorage.setItem('spID',id);
    this.openEdit();
  }
  openEdit() {
    this.modlService.open(UpdateServiceProviderComponent, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    //
  }
  onDelete(id){
    this.userSvc.deleteProvider(id).subscribe(x =>{
      console.log(x);
      setTimeout(() =>{
        location.reload();
      },3000);
      this.modlService.dismissAll();
      return this.toastr.success('Service Provider Deleted!', 'Success!');

    })
  }
  

}
