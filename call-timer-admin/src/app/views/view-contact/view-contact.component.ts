import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../../data/user-service.service';
import { IContact } from '../../data/imodel';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-contact',
  templateUrl: './view-contact.component.html',
  styleUrls: ['./view-contact.component.scss']
})
export class ViewContactComponent implements OnInit {
  private contactlist: IContact[];
  private name: string;

  constructor(private userSvc: UserServiceService,private modlService: NgbModal) { }

  ngOnInit() {
    this.getContact();
    this.getName();
  }
  getContact(){
        this.userSvc.getContactList().subscribe( data => {
        console.log(data);
        this.contactlist = data;
      });
  
  }
  close(){
    this.modlService.dismissAll();
  }
getName(){
  this.userSvc.getContactName().subscribe(x =>{
    this.name = x;
    console.log(this.name);
  })
}
}
