import { Component, ViewContainerRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserServiceService } from '../../data/user-service.service';
import { Router } from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import { SnackbarService } from 'ngx-snackbar';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  myForm: FormGroup;

  constructor(private userSvc: UserServiceService, private formBuilder: FormBuilder,
     private router: Router,private toastr: ToastrService,private snackbarService: SnackbarService) {
       this.createForm();
      }


 createForm() {
    this.myForm = this.formBuilder.group({
      username: ['', [Validators.required,
       ]],
        password: ['', [Validators.required]],
    });
  }
  submitForm () {
    if (!this.myForm.valid) {
      console.log(this.myForm.value)
      return this.toastr.error('Please fill in the right admin credentials!', 'Oops!');
    }
    this.userSvc.login(this.myForm.value).subscribe(data => {
      localStorage.setItem('token', data['token'] );
      this.router.navigate(['/dashboard']);
    });
  }
  showSuccess() {
    this.toastr.success('You are awesome!', 'Success!');
  }

  showError() {
    this.toastr.error('This is not good!', 'Oops!');
  }

  showWarning() {
    this.toastr.warning('You are being warned.', 'Alert!');
  }

  showInfo() {
    this.toastr.info('Just some information for you.');
  }
  


 }
