import { Injectable } from '@angular/core';
import { IUser, ILogin, IServiceProvider, IContact, ICallLog, IForgotPassword } from './imodel';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  private apiUrl = environment.apiUrl;
  private _registerController = 'register';
  private _providerController = 'serviceprovider';

  private _login = 'admin';
  private _email = 'register/email';
  private _delete = 'Register/Delete/';
  private _deleteProvider = 'serviceProvider/Delete/';

  private _serviceProvider = 'serviceProvider';
  private _serviceProviderById = 'serviceProviderById';
  private _contact = 'contact';
  private _userContact = 'getContact';
  private _contactDelete = 'Contact/Delete/';
  private _callLog = 'CallLog';
  private _orderByDate = 'CallLog/orderByDate';
  private _orderByDuration = 'CallLog/orderByDuration';
  private _generatePasswordCode = '/generatePasswordCode';
  private _resetPassword = '/resetPassword';
  private _getAllUsers = 'register/allUsers';
  private databaseReady: BehaviorSubject<boolean>;
  private contactlist = new Subject<IContact[]>();
  private UpdatedContact = new Subject<IContact>();
  private userEmail = new  BehaviorSubject<string>('');
  private contactName = new BehaviorSubject<string>('');

  private _AllUserContacts = 'Contact/AllUserContacts'

  constructor(private httpClient: HttpClient) {
    this.databaseReady = new BehaviorSubject(false);
   }

  saveUser(user: IUser): Observable<string> {
    return this.httpClient.post<string>(this.apiUrl + this._registerController, user);
  }
  saveServiceProvider(provider: IServiceProvider): Observable<string>{
    return this.httpClient.post<string>(this.apiUrl + this._providerController, provider);

  }
  login(login: ILogin) {
    return this.httpClient.post(this.apiUrl + this._login, login);
  }
  getCurrentUser(userid: string): Observable<IUser> {
    return this.httpClient.get<IUser>(this.apiUrl + this._registerController + '?id=' + userid);
  }
  getAllUsers(): Observable<IUser[]> {
    return this.httpClient.get<IUser[]>(this.apiUrl + this._getAllUsers);
  }
  delete(email: string) {
    return this.httpClient.delete(this.apiUrl + this._delete + email);

  }
  deleteProvider(id: string) {
    return this.httpClient.delete(this.apiUrl + this._deleteProvider + id);

  }
  getServiceProvider() {
    return this.httpClient.get<IServiceProvider[]>(this.apiUrl + this._serviceProvider);
  }
  getServiceProviderById(id: string) {
    return this.httpClient.get<IServiceProvider>(this.apiUrl + this._serviceProvider + '/' + this._serviceProviderById + '/' + id);
  }
  saveContact(contact: IContact): Observable<IContact> {
    this.databaseReady.next(true);
    return this.httpClient.post<IContact>(this.apiUrl + this._contact, contact);
  }
  getContactById(id: string): Observable<IContact[]> {
  //  this.databaseReady.next(true);
    return this.httpClient.get<IContact[]>(this.apiUrl + this._contact + '/' + this._userContact + '?id=' + id);

  }
  getContacts(): Observable<IContact[]> {
    //  this.databaseReady.next(true);
      return this.httpClient.get<IContact[]>(this.apiUrl + this._AllUserContacts);
  
    }
  getContactId(id: string) {
    //  this.databaseReady.next(true);
      return this.httpClient.get(this.apiUrl + this._contact + '?id=' + id);
  
    }
  getDatabaseState() {
    return this.databaseReady.asObservable();
  }

  setContactList(data: IContact[]) {
    this.contactlist.next(data);
    console.log(data)
  }
  setContactName(data: string) {
    this.contactName.next(data);
    console.log(data)
  }
  getContactName() : Observable<string>{
    return this.contactName.asObservable();

  }


  getContactList() : Observable<IContact[]> {
    return this.contactlist.asObservable();
  }
  
  clearContactList() {
    this.contactlist.next();
  }

  deleteContact(id: string) {
    return this.httpClient.delete(this.apiUrl + this._contactDelete + id);
  }

  setUpdatedContact(data: IContact) {
    this.UpdatedContact.next(data);
    console.log(data)
  }

  getUpdatedContact() : Observable<IContact> {
    return this.UpdatedContact.asObservable();
  }
  saveCallLog(callLog: ICallLog): Observable<IContact> {
    return this.httpClient.post<IContact>(this.apiUrl + this._callLog, callLog);
  }


  generatePasswordCode(data: IForgotPassword): Observable<IForgotPassword> {
    return this.httpClient.post<IForgotPassword>(this.apiUrl + this._registerController + this._generatePasswordCode, data);
  }

  resetPassword(data: IForgotPassword): Observable<IForgotPassword> {
    return this.httpClient.post<IForgotPassword>(this.apiUrl + this._registerController + this._resetPassword, data);
  }

  setUserEmail(data: string) {
    this.userEmail.next(data);
  }

  getUserEmail(): Observable<string> {
    return this.userEmail.asObservable();
  }

  clearUserEmail() {
    this.userEmail.next('');
  }
  getCallLog(){
    return this.httpClient.get<ICallLog[]>(this.apiUrl + this._callLog);
  }
  getOrderByDate(){
    return this.httpClient.get<ICallLog[]>(this.apiUrl + this._orderByDate);
  }
  getOrderByDuration(){
    return this.httpClient.get<ICallLog[]>(this.apiUrl + this._orderByDuration);
  }
}
