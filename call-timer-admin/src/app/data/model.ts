import { IUser, ILogin, IServiceProvider, IContact, ICallLog, IForgotPassword } from './imodel';

export class User implements IUser {
    id = '';
    firstName = '';
    lastName = '';
    email = '';
    mobileNo = '';
    serviceProviderID = '';
    password = '';
    serviceProviderName = '';
    idNumber = '';
}
export class Login implements ILogin {
username = '';
password = '';
}
export class ServiceProvider implements IServiceProvider {
    id = '';
    serviceProviderName = '';
    serviceProviderUSSD = '';
}
export class Contact implements IContact {
    id = '';
    userId = '';
    contactName = '';
    contactNumber = '';
}
export class CallLog implements ICallLog {
    id = '';
    callFromName = '';
    callFromNumber = '';
    callToName = '';
    callToNumber  = '';
    callDuration  = '';
    callDate = '';
}

export class ForgotPassword implements IForgotPassword {
    email = '';
    mobileNo = '';
    passwordResetCode = '';
    passwordResetTime = '';
    newPassword = '';
}

