export interface IUser {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    mobileNo: string;
    serviceProviderID: string;
    password: string;
    serviceProviderName: string;
    idNumber: string;
}
export interface ILogin {
    username: string;
    password: string;
}
export interface IServiceProvider {
    id: string;
    serviceProviderName: string;
    serviceProviderUSSD: string;
}

export interface IContact {
    id: string;
    userId: string;
    contactName: string;
    contactNumber: string;
}
export interface ICallLog {
    id: string;
    callFromName: string;
    callFromNumber: string;
    callToName: string;
    callToNumber : string;
    callDuration : string;
    callDate: string;
}

export interface IForgotPassword {
    email: string;
    mobileNo: string;
    passwordResetCode: string;
    passwordResetTime: string;
    newPassword: string;
}