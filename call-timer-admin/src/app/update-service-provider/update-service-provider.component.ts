import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IServiceProvider } from '../data/imodel';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserServiceService } from '../data/user-service.service';

@Component({
  selector: 'app-update-service-provider',
  templateUrl: './update-service-provider.component.html',
  styleUrls: ['./update-service-provider.component.scss']
})
export class UpdateServiceProviderComponent implements OnInit {

  closeResult: string;
  myForm: FormGroup;
  serviceProviders: IServiceProvider[];

  constructor(private modlService: NgbModal,private formBuilder: FormBuilder,private toastr: ToastrService,
    private userSvc: UserServiceService) { }

  ngOnInit() {
    this.createForm();
    this. getServiceProvider();
    this.getProvider();
  }
  open(content) {
    this.modlService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      id: [''],
      serviceProviderName: ['', [Validators.required,
       ]],
       serviceProviderUSSD: ['', [Validators.required]],
    });
  }
  submitForm () {
    if (!this.myForm.valid) {
      console.log(this.myForm.value)
      return this.toastr.error('Please fill in all fields!', 'Oops!');
    }
    this.userSvc.saveServiceProvider(this.myForm.value).subscribe(x =>{
      console.log(x);
      setTimeout(() =>{
        location.reload();
      },3000);
      this.modlService.dismissAll();

       this.toastr.success('Service Provider Updated!', 'Success!');

    })
  }
  getServiceProvider() {
    this.userSvc.getServiceProvider().subscribe(data => {
      console.log(data);
      this.serviceProviders = data;
    })
  }
  getProvider(){
    this.userSvc.getServiceProviderById(localStorage.getItem('spID')).subscribe(x =>{
      this.loadDataFromDatabase(x);

    })
  }
  loadDataFromDatabase(provider: IServiceProvider) {
    console.log(provider);
    this.myForm.patchValue({
                              id: provider.id,
                              serviceProviderName: provider.serviceProviderName,
                              serviceProviderUSSD: provider.serviceProviderUSSD,

                              
    });
  }
  diss(){
    this.modlService.dismissAll();
  }
}
